Pod::Spec.new do |m|

  version = '1.0.0'

  m.name    = 'Steerpath-Map-SDK'
  m.version = version

  m.summary           = 'Modified version of the Mapbox iOS SDK created by Steerpath'
  m.description       = 'Map SDK that can be used to access Steerpath hosted map data'
  m.homepage          = 'https://sdk.steerpath.com'
  m.author            = { 'Steerpath' => 'support@steerpath.com' }
  m.documentation_url = 'https://www.mapbox.com/ios-sdk/api/'
  m.license           = 'BSD'

  m.source = {
    :http => "https://s3-eu-west-1.amazonaws.com/steerpath/ios/releases/map-sdk/steerpath-map-sdk-1.0.0.zip",
    :flatten => true
  }

  m.platform              = :ios
  m.ios.deployment_target = '8.0'

  m.requires_arc = true

  m.vendored_frameworks = 'dynamic/Mapbox.framework'
  m.module_name = 'Mapbox'

end
