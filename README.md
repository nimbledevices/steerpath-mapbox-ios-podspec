# Copyright Steerpath Ltd. 2019. All rights reserved

Steerpath iOS Map SDK.

A Steerpath fork of the Mapbox iOS SDK. Used for accessing Steerpath
hosted map data. You can not use Mapbox online services with this product as they are disabled.

# How to update PodSpec (for Steerpath Developers)

1. If this is your first time updating the PodSpec you need to do the following:

- Clone the examples repository: https://bitbucket.org/nimbledevices/steerpath-smart-sdk-ios-examples
- Follow the instructions in the repository and build one of the examples projects.
- This will download the required CocoaPod dependencies to your system.

2. Replace the **Steerpath-Map-SDK.podspec.json** file with your updated file.
3. Run the following command in Terminal:
```
./publish.sh
```
4. You will be prompted with a list of local CocoaPod repositories similar to the one below. Select (type/copy paste) the repository you're updating and hit enter.
```
Attempting to list local cocoapod repositories: 

bitbucket-nimbledevices-steerpath-sdk-ios-podspec
master
bitbucket-nimbledevices-steerpath-smart-sdk-podspec
steerpath-map-sdk-bitbucket


Select repository for publish: 
```

5. You will need to confirm the repository with the next prompt.
```
Are you sure you want to publish to steerpath-map-sdk-bitbucket? (y/n): 
```

6. Publishing/uploading will run. After completion pull and push the changes into git.

7. Great success!

# Feedback, Support & Suggestions
* Contact: support@steerpath.com